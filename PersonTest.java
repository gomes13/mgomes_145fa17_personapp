
import java.util.Scanner;

// include any necessary import statements

/**
 * Application (driver) class used to test the capabilities of class Person
 *
 * @author
 * @version
 */
public class PersonTest {

    public static void main(String[] args) {
        // instantiate Scanner object for reading in keyboard input
        
        Scanner input = new Scanner( System.in );
        // prompt user to input name
        
        System.out.print("Enter the first person's name: ");
        // read in the name 
        
        String name1 = input.nextLine();
        // prompt user to input SSN
        
        System.out.print("Enter the first person's 9 didgit SSN (no spaces or dashes): ");
        // read in the SSN
        
        int ssn1 = input.nextInt();
        // prompt user to input voter registration status as true or false
        
        System.out.print("Enter the first person's Voter Registration status (true or false): ");
        // read in the voter status 
        
        boolean voterStatus1 = input.nextBoolean();
        // prompt user to input annual salary
        
        System.out.print("Enter the first person's Annual Salary as a decimal value: ");
        // read in the salary amount
        double salary1 = input.nextDouble();
        
        // Create new Person object using the parameters above, and assign 
        // object reference to newly declared person1 object variable
        Person person1 = new Person( name1, ssn1, voterStatus1, salary1);


        // Create new Person object using the parameters given in the
        // homework instructions, and assign object reference to person2
        Person person2 = new Person ("Barry Obama", 987654321, false, -123.45);
        // Print a blank line
        System.out.println();
        // Display person1's information
        person1.displayInfo();
        // Print a blank line
        System.out.println();
        // Display person2's information
        person2.displayInfo();
        // update person2's information by calling each of the
        // four instance methods
        // Print a blank line
        // Print a message indicating that person2's info has been changed
        // Print a blank line
        // Display person2's updated information 
    } // end method main

} // end class PersonTest

