
/**
 * Class used to define Person objects, which have the following attributes:
 * name, social security number, voter registration status, and annual salary
 *
 * @author
 * @version
 */
public class Person {

    /*
    * FIELD(S)
    */
    private String name;  // initializes to null
    private int socialSecurityNumber; // initializes to 0
    private boolean voterRegistrationStatus; // initializes to false
    private double annualSalary; // initializes to 0.0

    /*
     * CONSTRUCTOR(S)
     */
    public Person(String name,
            int socialSecurityNumber,
            boolean voterRegistrationStatus,
            double annualSalary) {
        this.name = name;
        this.socialSecurityNumber = socialSecurityNumber;
        this.voterRegistrationStatus = voterRegistrationStatus;
       
        if (annualSalary>=0.0){
        this.annualSalary = annualSalary;
        } // end if 
                
    } // END PERSON CONSTRUCTOR 
    
    
     /*
     * METHOD(S)
     */
    /** 
     * Modifies the person object's name
     * @ param name 
     */
    public void setName (String name) {
        this.name = name;
    }// end method setName
    
    /** 
     * Returns the Person object's name
     * @return name
     */
    public String getName(){
        return name;
    }// end method getName

    /**
     * @return the socialSecurityNumber
     */
    public int getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * @param socialSecurityNumber the socialSecurityNumber to set
     */
    public void setSocialSecurityNumber(int socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * @return the voterRegistrationStatus
     */
    public boolean getVoterRegistrationStatus() {
        return voterRegistrationStatus;
    }

    /**
     * @param voterRegistrationStatus the voterRegistrationStatus to set
     */
    public void setVoterRegistrationStatus(boolean voterRegistrationStatus) {
        this.voterRegistrationStatus = voterRegistrationStatus;
    }

    /**
     * @return the annualSalary
     */
    public double getAnnualSalary() {
        return annualSalary;
    }

    /**
     * @param annualSalary the annualSalary to set
     */
    public void setAnnualSalary(double annualSalary) {
        this.annualSalary = annualSalary;
    }
    
    
    /**
     * Displays the current Person object's information on 4 separate lines
     */
    public void displayInfo() {
        System.out.printf( "Name: %s\nSSN: %d\nVoter Status: %b\nSalary %.2f\n",
                           getName(),
                           getSocialSecurityNumber(),
                           getVoterRegistrationStatus(),
                           getAnnualSalary() );
    } // end method displayInfo

} // end class Person

